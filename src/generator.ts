import * as ts from 'typescript';
import * as fs from 'fs-extra';
import * as path from 'path';
import * as _ from 'lodash';

import {OpenApiDefinition} from './typings';
import {runPreprocess} from './preprocess';
import {
	createFileName, createFunctionName, createOptionName, createOptionsType,
	createTypeName,
	getNodeTypeForProperty, mkdirp,
} from './utils';


declare interface ResourceApiInfo
{
	apiVersion: Array<string>,
	kind: Array<string>,
}


export async function generateApi(branch: string, outDir: string): Promise<void>
{
	const defs = await runPreprocess(branch);
	const files: Array<string> = [];

	for (let name in defs) {
		if (defs.hasOwnProperty(name)) {
			files.push(await processDefinition(name, defs[name], outDir));
		}
	}

	await processIndex(
		path.join(outDir, 'index.ts'),
		files.map((file) => './' + path.basename(file, '.ts')),
	);
}

async function processIndex(indexFile: string, files: Array<string>): Promise<void>
{
	process.stdout.write(`Writing index file ${indexFile}...`);

	const result = printFile(indexFile, files.map((name): ts.ExportDeclaration => {
		return ts.createExportDeclaration(
			[],
			[],
			undefined,
			ts.createStringLiteral(name),
		);
	}));

	await fs.writeFile(indexFile, result, {encoding: 'utf8'});

	console.log(' done');
}

async function processDefinition(name: string, def: OpenApiDefinition, outDir: string): Promise<string>
{
	process.stdout.write(`Processing ${name}...`);

	const typeName = createTypeName(name);
	const fileName = createFileName(name, outDir);
	const functionName = createFunctionName(name);
	const optionsType = createOptionsType(name);

	const imports: Array<string> = [typeName];
	const apiInfo = getResourceApiVersionAndKind(def);

	const options = createOptions(def, apiInfo, optionsType, imports);
	const fn = createFunction(functionName, typeName, optionsType, def, apiInfo);

	const result = printFile(fileName, [
		createImports('@kubernetes/client-node', imports),
		options,
		fn,
	]);

	await mkdirp(path.dirname(fileName));
	await fs.writeFile(fileName, result, {encoding: 'utf8'});

	console.log(' done');

	return fileName;
}

function createImports(from: string, imports: Array<string>): ts.ImportDeclaration
{
	imports = imports.filter((item, pos) => {
		return imports.indexOf(item) === pos;
	});

	return ts.createImportDeclaration(
		[],
		[],
		ts.createImportClause(undefined, ts.createNamedImports(imports.map((name) => {
			return ts.createImportSpecifier(undefined, ts.createIdentifier(name));
		}))),
		ts.createStringLiteral(from),
	);
}

function createOptions(def: OpenApiDefinition, apiInfo: ResourceApiInfo, typeName: string, imports: Array<string>): ts.InterfaceDeclaration
{
	const required = Array.isArray(def.required) ? def.required : [];
	const members: Array<ts.TypeElement> = _
		.map(def.properties || {}, (prop, name) => {
			if (name === 'apiVersion' && apiInfo.apiVersion.length === 1) {
				return;
			}

			if (name === 'kind' && apiInfo.kind.length === 1) {
				return;
			}

			let type: ts.TypeNode = getNodeTypeForProperty(prop, imports);

			if (name === 'apiVersion' && apiInfo.apiVersion.length > 1) {
				type = ts.createUnionTypeNode(apiInfo.apiVersion.map((version) => {
					return ts.createLiteralTypeNode(ts.createStringLiteral(version));
				}));
			}

			const questionToken: ts.QuestionToken|undefined = required.indexOf(name) >= 0 ?
				undefined :
				ts.createToken(ts.SyntaxKind.QuestionToken);

			return ts.createPropertySignature(
				[],
				createOptionName(name),
				questionToken,
				type,
				undefined,
			);
		})
		.filter((member) => typeof member !== 'undefined');

	return ts.createInterfaceDeclaration(
		[],
		[
			ts.createToken(ts.SyntaxKind.ExportKeyword),
			ts.createToken(ts.SyntaxKind.DeclareKeyword),
		],
		ts.createIdentifier(typeName),
		[],
		[],
		members,
	);
}

function getResourceApiVersionAndKind(def: OpenApiDefinition): ResourceApiInfo
{
	const info: ResourceApiInfo = {
		apiVersion: [],
		kind: [],
	};

	if (Array.isArray(def['x-kubernetes-group-version-kind'])) {
		def['x-kubernetes-group-version-kind'].forEach((v) => {
			const apiVersion: Array<string> = [];
			if (v.group !== '') {
				apiVersion.push(v.group);
			}

			apiVersion.push(v.version);

			info.apiVersion.push(apiVersion.join('/'));
			info.kind.push(v.kind);
		});

		info.apiVersion = _.uniq(info.apiVersion);
		info.kind = _.uniq(info.kind);
	}

	return info;
}

function createFunction(functionName: string, resourceType: string, optionsType: string, def: OpenApiDefinition, apiInfo: ResourceApiInfo): ts.FunctionDeclaration
{
	const properties = def.properties || {};

	const optionsList = _.map(properties, (prop, name) => {
		return createOptionName(name);
	});

	const setup: Array<ts.Statement> = [];

	if (apiInfo.apiVersion.length === 1) {
		setup.push(ts.createExpressionStatement(
			ts.createBinary(
				ts.createPropertyAccess(ts.createIdentifier('resource'), ts.createIdentifier('apiVersion')),
				ts.createToken(ts.SyntaxKind.EqualsToken),
				ts.createStringLiteral(apiInfo.apiVersion[0]),
			),
		));
	}

	if (apiInfo.kind.length === 1) {
		setup.push(ts.createExpressionStatement(
			ts.createBinary(
				ts.createPropertyAccess(ts.createIdentifier('resource'), ts.createIdentifier('kind')),
				ts.createToken(ts.SyntaxKind.EqualsToken),
				ts.createStringLiteral(apiInfo.kind[0]),
			),
		));
	}

	const conditions: Array<ts.IfStatement> = optionsList
		.filter((option) => {
			if (option === 'apiVersion' && apiInfo.apiVersion.length === 1) {
				return false;
			}

			if (option === 'kind' && apiInfo.kind.length === 1) {
				return false;
			}

			return true;
		})
		.map((option) => {
			return ts.createIf(
				ts.createBinary(
					ts.createTypeOf(ts.createPropertyAccess(ts.createIdentifier('options'), ts.createIdentifier(option))),
					ts.createToken(ts.SyntaxKind.ExclamationEqualsEqualsToken),
					ts.createStringLiteral('undefined'),
				),
				ts.createBlock([
					ts.createExpressionStatement(
						ts.createBinary(
							ts.createPropertyAccess(ts.createIdentifier('resource'), ts.createIdentifier(option)),
							ts.createToken(ts.SyntaxKind.EqualsToken),
							ts.createPropertyAccess(ts.createIdentifier('options'), ts.createIdentifier(option)),
						),
					),
				]),
			);
		});

	const initializer: ts.Expression|undefined = Array.isArray(def.required) && def.required.length > 0 ?
		undefined :
		ts.createObjectLiteral([], false);

	return ts.createFunctionDeclaration(
		[],
		[ts.createToken(ts.SyntaxKind.ExportKeyword)],
		undefined,
		functionName,
		[],
		[ts.createParameter(
			[],
			[],
			undefined,
			'options',
			undefined,
			ts.createTypeReferenceNode(ts.createIdentifier(optionsType), []),
			initializer,
		)],
		ts.createTypeReferenceNode(ts.createIdentifier(resourceType), []),
		ts.createBlock([
			ts.createVariableStatement([], ts.createVariableDeclarationList([
				ts.createVariableDeclaration(
					ts.createIdentifier('resource'),
					undefined,
					ts.createNew(ts.createIdentifier(resourceType), [], []),
				),
			], ts.NodeFlags.Const)),
			...setup,
			...conditions,
			ts.createReturn(ts.createIdentifier('resource')),
		], true),
	);
}

function printFile(filename: string, statements: Array<ts.Statement>): string
{
	const file = ts.createSourceFile(filename, '', ts.ScriptTarget.Latest, false, ts.ScriptKind.TS);
	const printer = ts.createPrinter({
		newLine: ts.NewLineKind.LineFeed,
	});

	file.statements = ts.createNodeArray(statements);

	return printer.printNode(ts.EmitHint.Unspecified, file, file);
}
