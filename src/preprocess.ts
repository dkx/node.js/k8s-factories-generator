import {spawn} from 'child_process';
import * as path from 'path';
import * as fs from 'fs-extra';

import {downloadFile} from './utils';
import {OpenApiDefinitionsList} from './typings';


export async function runPreprocess(branch: string): Promise<OpenApiDefinitionsList>
{
	const temp = path.join(__dirname, '..', 'temp');
	const specPath = path.join(temp, 'open-api.json');

	try {
		await fs.access(specPath, fs.constants.F_OK);
		console.log(`Skipping preprocess, spec file already exists`);
	} catch (e) {
		await doRunPreprocess(branch, temp, specPath);
	}

	const data = JSON.parse(await fs.readFile(specPath, {encoding: 'utf8'}));
	return data.definitions;
}

async function doRunPreprocess(branch: string, temp: string, specPath: string): Promise<void>
{
	const preprocessPath = path.join(temp, 'preprocess_spec.py');
	const customObjectsPath = path.join(temp, 'custom_objects_spec.json');

	try {
		await fs.mkdir(temp);
	} catch (e) {}

	await downloadFile(
		'https://raw.githubusercontent.com/kubernetes-client/gen/master/openapi/preprocess_spec.py',
		preprocessPath,
	);

	await downloadFile(
		'https://raw.githubusercontent.com/kubernetes-client/gen/master/openapi/custom_objects_spec.json',
		customObjectsPath,
	);

	process.stdout.write('Running preprocess...');
	await new Promise((resolve, reject) => {
		const preprocess = spawn('python3', [
			preprocessPath,
			'typescript',
			branch,
			specPath,
			'kubernetes',
			'kubernetes',
		]);

		preprocess.stderr.on('data', (data) => {
			process.stderr.write(data);
		});

		preprocess.on('close', (code) => {
			if (code !== 0) {
				console.log(` fail (${code})`);
				return reject();
			}

			console.log(' done');
			resolve();
		});
	});

	await fs.unlink(preprocessPath);
	await fs.unlink(customObjectsPath);
	await fs.unlink(specPath + '.unprocessed');
}
