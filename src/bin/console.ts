#!/usr/bin/env node

import * as path from 'path';
import * as yargs from 'yargs';

import {generateApi} from '../generator';


yargs
	.command('gen <branch> <outDir>', 'Generate factories', (yargs) => {
		return yargs
			.positional('branch', {
				type: 'string',
				description: 'Kubernetes branch to use, eg. release-1.15',
			})
			.positional('outDir', {
				type: 'string',
				description: 'Output directory',
			});
	}, async (argv) => {
		const outDir = path.isAbsolute(argv.outDir) ?
			argv.outDir :
			path.join(process.cwd(), argv.outDir);

		await generateApi(argv.branch, outDir);
	})
	.demandCommand()
	.argv;
