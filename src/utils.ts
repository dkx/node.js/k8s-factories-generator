import fetch from 'node-fetch';
import * as mkdirRecursive from 'mkdirp';
import * as path from 'path';
import * as ts from 'typescript';
import * as fs from 'fs-extra';

import {OpenApiDefinitionProperty} from './typings';


export function firstUpper(str: string): string
{
	return str.charAt(0).toUpperCase() + str.slice(1);
}

export function firstLower(str: string): string
{
	return str.charAt(0).toLowerCase() + str.slice(1);
}

export async function downloadFile(url: string, destination: string): Promise<void>
{
	process.stdout.write(`Downloading ${url}...`);
	const res = await fetch(url);
	const content = await res.text();

	await fs.writeFile(destination, content, {encoding: 'utf8'});
	console.log(' done');
}

export function mkdirp(dir: string): Promise<void>
{
	return new Promise((resolve, reject) => {
		mkdirRecursive(dir, (err) => {
			if (err) {
				return reject(err);
			}

			resolve();
		});
	});
}

export function createOptionName(name: string): string
{
	if (name === 'default') {
		return '_default';
	}

	if (name === 'continue') {
		return '_continue';
	}

	if (name === 'enum') {
		return '_enum';
	}

	name = name.replace(/-/g, '_');

	return name;
}

export function createTypeName(name: string): string
{
	return name
		.split('.')
		.map((part) => firstUpper(part))
		.join('');
}

export function createFileName(name: string, outDir: string): string
{
	name = name
		.split('.')
		.map((part) => firstUpper(part))
		.join('');

	return path.join(outDir, firstLower(name) + '.ts');
}

export function createFunctionName(name: string): string
{
	return 'create' + name
		.split('.')
		.map((part) => firstUpper(part))
		.join('');
}

export function createOptionsType(name: string): string
{
	return name
		.split('.')
		.map((part) => firstUpper(part))
		.join('') + 'Options';
}

export function getNodeTypeForProperty(prop: OpenApiDefinitionProperty, imports: Array<string>): ts.TypeNode
{
	if (typeof prop.$ref === 'string') {
		const referenceType = createTypeName(prop.$ref.substring(14));

		imports.push(referenceType);
		return ts.createTypeReferenceNode(ts.createIdentifier(referenceType), []);
	}

	if (prop.type === 'array') {
		if (typeof prop.items === 'undefined') {
			throw new Error('Missing inner type for array');
		}

		return ts.createArrayTypeNode(
			getNodeTypeForProperty(prop.items, imports),
		);
	}

	if (prop.type === 'object') {
		if (typeof prop.additionalProperties === 'undefined') {
			return ts.createKeywordTypeNode(ts.SyntaxKind.ObjectKeyword);
		}

		return ts.createTypeLiteralNode([
			ts.createIndexSignature(
				[],
				[],
				[
					ts.createParameter(
						[],
						[],
						undefined,
						ts.createIdentifier('key'),
						undefined,
						ts.createKeywordTypeNode(ts.SyntaxKind.StringKeyword),
						undefined,
					),
				],
				getNodeTypeForProperty(prop.additionalProperties, imports),
			),
		]);
	}

	if (prop.type === 'string') {
		if (prop.format === 'date-time') {
			return ts.createTypeReferenceNode(ts.createIdentifier('Date'), []);
		}

		return ts.createKeywordTypeNode(ts.SyntaxKind.StringKeyword);
	}

	if (prop.type === 'integer' || prop.type === 'number') {
		return ts.createKeywordTypeNode(ts.SyntaxKind.NumberKeyword);
	}

	if (prop.type === 'boolean') {
		return ts.createKeywordTypeNode(ts.SyntaxKind.BooleanKeyword);
	}

	throw new Error(`Can not create node type from "${prop.type}"`);
}
